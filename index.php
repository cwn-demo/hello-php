<!DOCTYPE html>
<html class="" lang="en">
<meta charset="utf-8">
<title>cwn-demo / hello-php</title>

</head>
<body>
    
    <main class="content" id="content-body" itemscope itemtype="http://schema.org/SoftwareSourceCode">

        <h1>Hello Worl</h1>
        
        <p><b>
<?php
print "PHP version is " . PHP_VERSION . "\n <br>"; 
print "HOSTNAME is " . getenv('HOSTNAME') . "\n <br>"; 
print "PHP_CONTAINER_SCRIPTS_PATH is " . getenv('PHP_CONTAINER_SCRIPTS_PATH') . "\n <br>"; 
print "HOME is " . getenv('HOME') . "\n <br>"; 
print "DB_NAME is " . getenv('DB_NAME') . "\n <br>"; 
?>

        </b></p>

<p>
            print "<b>DB_NAME is " . getenv('DB_NAME') . "\n</b> <br><br>"; 
            <?php
            // set connection string
//            $connection = "host=postgresql dbname=" . getenv('DB_NAME') . " user=" . getenv('DB_USR') . " password=" . getenv('DB_PWD');
            $connection = "host=postgresql dbname=hello user=dbadmin password=veryweak";
            // print $connection;

            // Connecting, selecting database
            $dbconn = pg_connect($connection)
                or die('Could not connect: ' . pg_last_error());

            // Performing SQL query
            $query = 'SELECT first_name, last_name FROM users ORDER BY id';
            $result = pg_query($query) or die('Query failed: ' . pg_last_error());

            // Printing results in HTML
            echo "<table>\n";
            while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
                echo "\t<tr>\n";
                foreach ($line as $col_value) {
                    echo "\t\t<td>$col_value</td>\n";
                }
                echo "\t</tr>\n";
            }
            echo "</table>\n";

            // Free resultset
            pg_free_result($result);

            // Closing connection
            pg_close($dbconn);
            ?>
        </p>

    </main>
</body>
</html>

